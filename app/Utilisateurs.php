<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilisateurs extends Model
{
	protected $table = "Utilisateurs";

    public $timestamps = false;
}
