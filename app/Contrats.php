<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrats extends Model
{
    protected $table = "Contrats";

    public $timestamps = false;

    protected $fillable=['idClient', 'coût', 'dateDébut', 'dateFin' , 'état'];
}