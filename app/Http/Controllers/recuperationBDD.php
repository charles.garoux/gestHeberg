<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class recuperationBDD extends Controller
{
    public function listerClients()
    {
    	$clients = DB::select('select * from Clients', [1]);
		return view('listeClients', compact('clients'));
    }

    public function listerContrats()
    {
    	$contrats = DB::select('select * from Contrats', [1]);
		return view('listeContrats', compact('contrats'));
    }
}