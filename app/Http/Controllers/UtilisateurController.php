<?php

namespace App\Http\Controllers;

use App\FicheFrais;
use App\Utilisateurs;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class UtilisateurController extends Controller
{
    public function connection(): object
    {
    	return view('formulaireConnection');
    }

    public function verification(Request $request): RedirectResponse
    {
    	$Utilisateur = Utilisateurs::select(["id", "nom", "prenom"])
    		->where("identifiant", "=", $request->identifiant)
    		->where("motDePasse", "=", $request->motDePasse)
    		->first();

    	if ($Utilisateur)
    	{
    		Session::put("Utilisateur", $Utilisateur);

    		Session::flash("success", "Bienvenue, $Utilisateur->nom $Utilisateur->prenom.");
            return redirect(route('menuPrincipale'));
    	}
    	else
    	{
    		Session::flash("error", "Utilisateur inconnu.");

    		return redirect(route('connection'));
    	}
    }

    public function deconnection(): RedirectResponse
    {
        Session::flush();
        return redirect("/");
    }
}
