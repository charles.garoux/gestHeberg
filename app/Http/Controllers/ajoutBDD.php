<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Clients;
use App\Contrats;
use Illuminate\Support\Facades\Session;



class ajoutBDD extends Controller
{
	public function formulaireClient()
	{
		return view('formulaireClient');
	}

	public function formulaireContrat()
	{
		return view('formulaireContrat');
	}

    public function ajoutClient(Request $request)
    {
    	$nom = $request->input('nom');
    	$numero = $request->input('numero');
    	$email = $request->input('email');

    	if($nom == null || $numero == null || $email == null)
    	{
    		return "Formulaire invalide!";
    	}

        Clients::create([
            "nom" => $nom,
            "numTel" => $numero,
            "email" => $email
        ]);

        Session::flash("information", "Un client a été ajouté.");

	    return redirect(route('listeClients'));
    }

    public function ajoutContrat(Request $request)
    {
    	$idClient = $request->input('id');
    	$coutM = $request->input('coutM');
        $dateDebut = $request->input('dateDebut');
        $état = $request->input('état');

    	if($idClient == null || $coutM == null || $dateDebut == null || $état == null)
    	{
    		return "Formulaire invalide!";
    	}

        Contrats::create([
            "idClient" => $idClient,
            "coût" => $coutM,
            "dateDébut" => $dateDebut,
            "état" => $état
        ]);

        Session::flash("information", "Un contrat a été ajouté.");

	    return redirect(route('listeContrats'));
    }
}