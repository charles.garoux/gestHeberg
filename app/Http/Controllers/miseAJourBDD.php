<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contrats;
use Illuminate\Support\Facades\Session;

class miseAJourBDD extends Controller
{
    public function changerEtatContrat(Request $request)
    {
    	$id = $request->input('idContrat');
    	$nouvelEtat = $request->input('nouvelEtat');


    	$contrat = Contrats::find($id);

    	if (!empty($id))
    	{
    		$contrat->état = $nouvelEtat;
    		if($nouvelEtat == 'Terminé')
    		{
    			$contrat->dateFin = now();
    		}
    		$contrat->save();

    		Session::flash("information", "L'état du contrat a bien été changé");
    	}
    	else
    	{
    		Session::flash("error", "Un problème est survenu lors de la modification de l'état du contrat.");
    	}
    	return redirect(route("listeContrats"));
    }
}
