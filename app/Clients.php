<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
	protected $table = "Clients";

    public $timestamps = false;

    protected $fillable=['nom', 'numTel', 'email'];
}
