<?php

Route::get("/connection", "UtilisateurController@connection")->name('connection');
Route::post("/connection", "UtilisateurController@verification")->name('connection');


Route::group(["middleware" => 'Auth'], function() {

	Route::get('/', function () {	return view('menuPrincipale');	})->name('menuPrincipale');

    Route::get('/listeClients', 'recuperationBDD@listerClients')->name('listeClients');
    Route::get('/ajoutClient', 'ajoutBDD@formulaireClient')->name('ajoutClient');
    Route::post('/ajoutClient', 'ajoutBDD@ajoutClient')->name('ajoutClient');


    Route::get('/listeContrats', 'recuperationBDD@listerContrats')->name('listeContrats');
    Route::get('/ajoutContrat', 'ajoutBDD@formulaireContrat')->name('ajoutContrat');
    Route::post('/ajoutContrat', 'ajoutBDD@ajoutContrat')->name('ajoutContrat');
    Route::post('/changerEtatContrat', 'miseAJourBDD@changerEtatContrat')->name('changerEtatContrat');

    Route::get("/deconnection", "UtilisateurController@deconnection")->name('deconnection');
});
