@extends('templates.connection')

@section('entete')
<h1>Connection à GestHeberg</h1>
@endsection

@section('contenu')
<div id="compteRendu" class="container-fluid">
    <div class="row justify-content-center">
        <form action="" method="post" class="col-xlg-2 col-lg-4 col-md-6 col-sm-12">
            <input type="hidden" name="_token" value"{{ csrf_field() }}
            <div class="form-group">
                <label for="identifiant">Identifiant</label>
                <input type="text" class="form-control" id="identifiant" name="identifiant" placeholder="">
                <small id="emailHelp" class="form-text text-muted">Ne partagez jamais vos information personnel.</small>
            </div>
            <div class="form-group">
                <label for="motDePasse">Mot de passe</label>
                <input type="password" class="form-control" id="motDePasse" name="motDePasse" placeholder="">
            </div>
            <button type="submit" class="btn btn-primary">Valider</button>
        </form>
    </div>
</div>
@endsection