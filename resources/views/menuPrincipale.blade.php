@extends('templates.base')

@section('entete')
<h1>Menu principale</h1>
@endsection

@section('contenu')
<h3>Présentation de l'application:</h3>
<p>
	gestHeberg permet de gérer une liste de clients et de contrats qui leur sont associés.<br>
	Ces contrats sont des contrats d'hébergements gérés par des étudiants de BTS SIO option SISR.
</p>
@endsection