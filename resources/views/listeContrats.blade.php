@extends('templates.base')

@section('entete')
<h1>Liste des Contrats</h1>
<a class="btn btn-success float-right" href="{{ route('ajoutContrat') }}">Ajouter un contrat</a>
@endsection

@section('contenu')
<table class="table">
	  <thead>
	    <tr>
	      	<th scope="col">#</th>
	       	<th scope="col">Date de début</th>
	      	<th scope="col">Date de fin</th>
	      	<th scope="col">Coût mensuel</th>
	      	<th scope="col">Etat actuel</th>
	      	<th scope="col"># client</th>
	      	<th scope="'col">Changement d'état</th>
	    </tr>
	</thead>
  	<tbody>
  	@foreach($contrats as $contrat)
  		<tr>
	    	<th scope="row">{{ $contrat->id }}</th>
	    	<td>{{ $contrat->dateDébut }}</td>
		    <td>{{ $contrat->dateFin }}</td>
		    <td>{{ $contrat->coût }}</td>
		    <td>{{ $contrat->état }}</td>
		    <td>{{ $contrat->idClient }}</td>
		    @if($contrat->état != "Terminé")
		    <form action="{{ route("changerEtatContrat") }}" method="POST">
                {{ csrf_field() }}
                <td>
                	<input type="hidden" name="idContrat" value="{{ $contrat->id }}">
	                <div class="form-group">
					    <select class="form-control" name="nouvelEtat">
					    	@if($contrat->état != "En attente" && $contrat->état != "En cours")
					    		<option value="En attente">En attente</option>
					    	@endif
					    	@if($contrat->état != "En cours")
					      		<option value="En cours">En cours</option>
					      	@endif
					      	<option value="Terminé">Terminé</option>
					    </select>
					</div>
					<button class="btn btn-danger btn-sm" type="submit">Valider le nouvel état</button>
                </td>
            </form>
            @endif
	    </tr>
	@endforeach()
  	</tbody>
</table>
@endsection