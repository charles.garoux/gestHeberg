@extends('templates.base')

@section('entete')
<h1>Liste des Clients</h1>
<a class="btn btn-success float-right" href="{{ route('ajoutClient') }}">Ajouter un client</a>
@endsection

@section('contenu')
<table class="table">
	  <thead>
	    <tr>
	      	<th scope="col">#</th>
	       	<th scope="col">Nom</th>
	      	<th scope="col">Numéro de téléphone</th>
	      	<th scope="col">Email</th>
	    </tr>
	</thead>
  	<tbody>
  	@foreach($clients as $client)
  		<tr>
	    	<th scope="row">{{ $client->id }}</th>
	    	<td>{{ $client->nom }}</td>
		    <td>{{ $client->numTel }}</td>
		    <td>{{ $client->email }}</td>
	    </tr>
	@endforeach()
  	</tbody>
</table>
@endsection