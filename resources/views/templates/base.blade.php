<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>GestHeberg</title>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <h1 class="navbar-brand">gestHeberg</h1>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          @if (Route::is('menuPrincipale'))
            <li class="nav-item active">
              <a class="nav-link" href="{{ route('menuPrincipale') }}">Menu principale</a>
            </li>
          @else
            <li class="nav-item">
              <a class="nav-link" href="{{ route('menuPrincipale') }}">Menu principale</a>
            </li>
          @endif
          @if (Route::is('listeClients'))
            <li class="nav-item active">
              <a class="nav-link" href="{{ route('listeClients') }}">Liste des clients</a>
            </li>
          @else
            <li class="nav-item">
              <a class="nav-link" href="{{ route('listeClients') }}">Liste des clients</a>
            </li>
          @endif
          @if (Route::is('listeContrats'))
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('listeContrats') }}">Liste des contrats</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('listeContrats') }}">Liste des contrats</a>
          </li>
          @endif
        </ul>
        <div class="my-2 my-lg-0">
          <a class="btn btn-danger" href="{{ route('deconnection') }}">Se déconnecter</a>
        </div>
      </div>
    </nav>

	  <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                    <a class="nav-link">
                        {{ Session::get("Utilisateur")->nom }}
                        {{ Session::get("Utilisateur")->prenom }}
                    </a>
                </li>
                
            </ul>
          </div>
        </nav>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="justify-content-md-center">
            <div class="card">
              <div class="card-body">
                @yield('entete')
              </div>
            </div>
            @include('flash')
            @yield('contenu')
          </div>
          @yield('piedsDePage')
        </main>
  	  	
      </div>
	  </div>

    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>