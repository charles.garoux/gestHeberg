@extends('templates.base')

@section('entete')
	<h1>Ajout d'un contrat:</h1>
@endsection

@section('contenu')
	<div class="input-group">
		<form action="" method="post">
			<input type="hidden" name="_token" value"{{ csrf_field() }}

			<label for="id">Identifiant du client:</label>
			<input type="text" class="form-control" name="id" id="id" />
			<label for="coutM">Coût mensuel:</label>
			<input type="text" class="form-control" name="coutM" id="coutM" />
			<label for="dateDebut">Date de début du contrat:</label>
			<input type="date" class="form-control" name="dateDebut" id="dateDebut">
			<label for="état">Etat du contrat à ça création:</label>
			<select class="form-control" name="état" id="état">
                    <option value="En attente">En attente</option>
                    <option value="En cours">En cours</option>
            </select>
            <button type="submit" class="btn btn-primary btn-lg float-right">Valider</button>
		</form>
	</div>
@endsection