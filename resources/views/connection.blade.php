@extends('templates.base')

@section('entete')
<h1>Connection à GestHeberg</h1>
@endsection

@section('contenu')
<div id="compteRendu" class="container-fluid">
    <div class="row justify-content-center">
        <form action=".php" method="post" class="col-xlg-2 col-lg-4 col-md-6 col-sm-12">
            <div class="form-group">
                <label for="identifiant">Identifiant</label>
                <input type="text" class="form-control" id="identifiant" placeholder="">
                <small id="emailHelp" class="form-text text-muted">Ne partagez jamais vos information personnel.</small>
            </div>
            <div class="form-group">
                <label for="MDP">Mot de passe</label>
                <input type="password" class="form-control" id="MDP" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-primary">Valider</button>
        </form>
    </div>
</div>
@endsection
