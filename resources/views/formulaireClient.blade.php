@extends('templates.base')

@section('entete')
	<h1>Ajout d'un client:</h1>
@endsection

@section('contenu')
	<div class="input-group">
		
		<form action="" method="post">
			<input type="hidden" name="_token" value"{{ csrf_field() }}

			<label for="nom">Nom du client:</label>
			<input type="text" class="form-control" name="nom" id="nom" />
			<label for="numero">Numéro de téléphone:</label>
			<input type="text" class="form-control" name="numero" id="numero" />
			<label for="email">Email:</label>
			<input type="text" class="form-control" name="email" id="email" />
			<button type="submit" class="btn btn-primary btn-lg float-right">Valider</button>
		</form>
	</div>
@endsection